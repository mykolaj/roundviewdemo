package com.mt53bureau.roundview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.mt53bureau.circleview.CircleView;

public class MainActivity extends AppCompatActivity {


    private CircleView mCircleView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mCircleView = (CircleView) findViewById(R.id.circle_view);
        mCircleView.addPartForPeriod(1, 6);
        mCircleView.addPartForPeriod(9, 11);
    }
}
