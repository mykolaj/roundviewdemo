package com.mt53bureau.circleview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class CircleView extends View {

    public static final int DEFAULT_RING_THICKNESS = 1;
    public static final int DEFAULT_RING_COLOR = Color.GREEN;
    private final int mRingThickness;
    private final int mRingColor;
    private final List<HourPart> mHourParts = new ArrayList<>();
    private final Rect mCanvasBounds = new Rect();
    private final Paint mRingPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    private static final float CLOCK_HOUR_STEP = (float) 360 / 12;

    public CircleView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CircleView,
                0, 0);

        if (a.hasValue(R.styleable.CircleView_ringThickness)) {
            mRingThickness = a.getDimensionPixelSize(R.styleable.CircleView_ringThickness,
                    DEFAULT_RING_THICKNESS);
        } else {
            mRingThickness = DEFAULT_RING_THICKNESS;
        }
        if (a.hasValue(R.styleable.CircleView_ringColor)) {
            mRingColor = a.getColor(R.styleable.CircleView_ringColor, DEFAULT_RING_COLOR);
        } else {
            mRingColor = DEFAULT_RING_COLOR;
        }
        a.recycle();
        init();
    }

    private void init() {
        mRingPaint.setStyle(Paint.Style.STROKE);
        mRingPaint.setColor(mRingColor);
        mRingPaint.setStrokeWidth(mRingThickness);
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);

        // Rotate the canvas so its zero angle match with the midnight mark on a clock
        canvas.rotate(90 * -1);

        canvas.getClipBounds(mCanvasBounds);
        final float halfStrokeThickness = (float) mRingThickness / 2;

        // TODO Instantiate this RectF before onDraw() method call and reuse the object
        RectF ringRect = new RectF(mCanvasBounds.left + halfStrokeThickness,
                mCanvasBounds.top + halfStrokeThickness,
                mCanvasBounds.right - halfStrokeThickness,
                mCanvasBounds.bottom - halfStrokeThickness);
        canvas.drawOval(ringRect, mRingPaint);


        if (!mHourParts.isEmpty()) {
            for (HourPart p : mHourParts) {
                drawRingArc(canvas, p);
            }
        }

        final float cx = ringRect.centerX();
        final float cy = ringRect.centerY();
        drawInsidePoints(canvas, cx, cy);

        // Rotate canvas back as it was from start, and draw text
        canvas.rotate(90);
        drawLabels(canvas);

    }

    private void drawLabels(final Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setTextSize(32);
        canvas.drawText("123", 32, 32, paint);
    }

    private void drawInsidePoints(final Canvas canvas, float cx, float cy) {
        // TODO Instantiate this Paint before onDraw() method call and reuse the object
        Paint pp = new Paint(Paint.ANTI_ALIAS_FLAG);
        pp.setColor(Color.BLACK);
        pp.setStyle(Paint.Style.FILL);
        pp.setStrokeJoin(Paint.Join.ROUND);
        pp.setStrokeCap(Paint.Cap.ROUND);
        pp.setStrokeWidth(16);

        final int canvasWidth = mCanvasBounds.bottom - mCanvasBounds.top;

        final int ringInnerMargin = 16;
        float innerRadius = ((float) canvasWidth / 2) - mRingThickness - ringInnerMargin;
        int[] pointPositions = new int[] {0, 3, 6, 9};

        for (int position : pointPositions) {
            final float angle = (float) position * CLOCK_HOUR_STEP;
            final float cos = (float) Math.cos(Math.toRadians(angle));
            final float sin = (float) Math.sin(Math.toRadians(angle));
            float x = cx + innerRadius * cos;
            float y = cy + innerRadius * sin;
            canvas.drawPoint(x, y, pp);
        }

    }

    private void drawRingArc(final Canvas canvas, final HourPart p) {
        final int start = p.startHour;
        final int end = p.endHour;
        final float startAngle = (float) start * CLOCK_HOUR_STEP;
        final float endAngle = (float) end * CLOCK_HOUR_STEP;

        final float halfStrokeThickness = (float) mRingThickness / 2;
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(mRingThickness);
        paint.setColor(Color.RED);

        // TODO Instantiate this RectF before onDraw() method call and reuse the object
        RectF r = new RectF(mCanvasBounds.left + halfStrokeThickness,
                mCanvasBounds.top + halfStrokeThickness,
                mCanvasBounds.right - halfStrokeThickness,
                mCanvasBounds.bottom - halfStrokeThickness);
        canvas.drawArc(r, startAngle, endAngle - startAngle, false, paint);
    }

    public void addPartForPeriod(int start, int end) {
        if (start < 0 || end < 0) {
            return;
        }
        if (start > 12 || end > 12) {
            return;
        }
        if (end == 0 || start == 12) {
            return;
        }
        if (start == end) {
            return;
        }

        final HourPart part = new HourPart();
        part.startHour = start;
        part.endHour = end;
        mHourParts.add(part);
        invalidate();
    }

    private static class HourPart {
        int startHour;
        int endHour;
    }


}
